# Анализ данных с Apache Spark
В этом руководстве я расскажу вам, как установить Apache Spark на машину под управлением Centos7.

Apache Spark — это быстрая и универсальная кластерная вычислительная система. Он предоставляет высокоуровневые API-интерфейсы на Java, Scala и Python, а также оптимизированный движок, поддерживающий общие диаграммы выполнения. Он также поддерживает богатый набор инструментов более высокого уровня, включая Spark SQL для SQL и обработки структурированной информации, MLlib для машинного обучения, GraphX ​​для обработки графиков и Spark Streaming.

Предполагается, что у вас есть хотя бы базовые знания Linux, вы знаете, как использовать оболочку. Установка довольно проста и предполагает, что вы работаете под учетной записью root или под учетной записью имеющей root права, в противном случае вам потребуется добавлять "sudo" к командам для получения привилегий root или использовать команду "su" для того чтобы выполнять команды от root пользователя.

## Подготовка машины
### Обновление системы
```bash
yum -y install epel-release 
yum -y update
```

### Отключение SELinux
Security- Enhanced Linux (SELinux) является особенностью механизма в Linux, что обеспечивает поддержку политик безопасности контроля доступа.

Чтобы отключить SELinux необходимо открыть config файл и изменить в нем переменную SELINUX на значение "disabled" (по умолчанию SELINUX=enforcing)

Чтобы открыть config файл

```bash
vi /etc/selinux/config
```

### Установка Java
Для работы Apache Spark требуется установка java

Сначала проверим, возможно java уже установлена

```bash
java -version
```

Если же вывелись ошибки, то значит что java не установлена

```bash
yum install java -y
```

### Установка Scala
Apache Spark реализован на языке программирования Scala, поэтому нам нужно установить Scala для запуска Apache Spark

```bash
wget https://www.scala-lang.org/files/archive/scala-2.13.4.tgz
tar xvf scala-2.13.4.tgz
mv scala-2.13.4 /usr/lib
ln -s /usr/lib/scala-2.13.4 /usr/lib/scala
export PATH=$PATH:/usr/lib/scala/bin
```

После установки проверьте версию scala

```bash
scala -version
```

### Установка Python
PySpark может использоваться для распределенных вычислений на Python в рамках анализа и обработки больших данных (Big Data), а также машинного обучения (Machine Learning).

Apache Spark реализован на языке программирования Scala, который выполняется на JVM (Java Virtual Machine). Чтобы получить функциональность Spark в Python, используется PySpark. Поэтому те, кто не знаком со Scala, но знаком с Python, могут запросто использовать возможности фрейвморка Apache Spark.

```bash
yum install gcc openssl-devel bzip2-devel
wget https://www.python.org/ftp/python/3.9.4/Python-3.9.4.tgz
tar xvf Python-3.9.4.tgz
./configure --enable-optimizations
./configure --enable-loadable-sqlite-extensions
make
make install
```

#### Установка библиотек
Для работы со Spark на Python необходимо установить библиотеки

 * Findspark - предоставляет init функцию, чтобы сделать библиотеку pyspark импортируемой
 * PySpark - используется для получения функционала Apache Spark в языке Python
 * Jupyter - инструмент для интерактивной разработки и представления проектов в области наук о данных.

```bash
pip3.9 install findspark
pip3.9 install pyspark
pip3.9 install jupyter
```

### Установка Spark
```bash
wget https://www.apache.org/dyn/closer.lua/spark/spark-3.1.2/spark-3.1.2-bin-hadoop3.2.tgz
tar xvf spark-3.1.2-bin-hadoop3.2.tgz
mkdir /spark/app
mkdir /spark/logs
mv spark-3.1.2-bin-hadoop3.2/* /spark/app/
```

#### Настройка Spark
##### Логирование
Для того чтобы включить сборку логов необходимо в конфигурационный файл

```bash
vi /spark/app/conf/spark-defaults.conf
```

написать следующее:

```bash
spark.eventLog.enabled   true
spark.eventLog.dir  /spark/logs/
spark.history.fs.logDirectory  /spark/logs
```
##### Реcурсы
Для управления количеством вычислительных ресурсов, которые будут предоставлены Spark необходимо изменить другой конфигурационный файл

```bash
vi /spark/app/conf/spark-env.conf
```

 * SPARK_WORKER_INSTANCES - количество узлов (параллельных процессов), которые будут выполнять задачи
 * SPARK_WORKER_CORES - количество ядер процессора, которое будет использовать каждый узел (по умолчанию используются все ядра)
 * SPARK_WORKER_MEMORY - количество оперативной памяти (в гигабайтах), предоставляемой кажому узлу

```bash
SPARK_WORKER_INSTANCES=4
SPARK_WORKER_CORES=2
SPARK_WORKER_MEMORY=2g
export PYSPARK_PYTHON=/usr/local/bin/python3.9
export PYSPARK_DRIVER_PYTHON=/usr/local/bin/puthon3.9
```
##### Добавление Spark в оболочку пользователя
Для того чтобы добавить Spark в переменные окружения необходимо создать profile файл
```bash
vi /etc/profile.d/spark.sh
```

и написать в нем путь до bin и sbin папки куда был установлен Spark
```bash
export SPARK_HOME=/spark/app
export PATH=$PATH:$SPARK_HOME/bin:/usr/local/bin:$SPARK_HOME/sbin
```

после этого запустить
```bash
source /etc/profile.d/spark.sh
source /etc/profile
```

### Запуск Spark
Для того чтобы запустить Apache Spark 

```bash
start-master.sh -h <your ip address>
start-slave.sh spark://<your ip address>:7077
start-history-server.sh
```
(Для выключения)

```bash
stop-history-server.sh
stop-slave.sh
stop-master.sh
```

По желанию можно написать скрипты чтобы осуществлять запуск и остановку Spark в одну команду или даже настроить автоматический запуск Apache Spark вместе с включением машины

### Создание рабочей директории
Для работы с ноутбуками jupyter лучше подготовить отдельную папку
```code bash
mkdir ~/workspace
cd ~/workspace
```

Для запуска jupyter необходимо ввести команду

```bash
jupyter notebook
```

После этого в браузере откроется стартовая страница, на которой будут представлены все файлы внутри директории в которой был запущен сервер

## Работа с PySpark
Более подробный пример работы с PySpark рассмотрен в прикрепленном ноутбуке nyc_analyze.ipynb

В примере проведен анализ csv файла, состоящего из 7 миллионов строк, содержащих записи о преступлениях в городе Нью-Йорк

### Старт сессии 
```Python
import findspark
findspark.init()
from  pyspark.sql import SparkSession

spark = SparkSession.builder.appName('My Spark project').getOrCreate()
dataFile = "path/to/your/file"
ny = spark.sparkContext.textFile(dataFile)
```

### Подсчет количества записей в файле
```Python
nyCount = ny.count()
print('Number of rows in csv file: {}'.format(nyCount))
```

### Получения списка названий столбцов в файле
```Python
header = ny.first()
field_names = header.replace(" ","_").split(",")
```

### Map function
В приведенном примере для удобного доступа к данным использовать коллекцию namedturple
```Python
Crime = namedtuple('Crime', field_names)

#map function
def parse(row):
    reader = csv.reader(StringIO(row))
    row = reader.__next__()
    return Crime(*row) 


ny_wo_header = ny.filter(lambda x: x != header)
crimes = ny_wo_header.map(parse)
```
