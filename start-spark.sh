#!/bin/bash
ip_address="0.0.0.0" #your ip address
echo "[INFO] Starting master"
start-master.sh -h $ip_address
echo "[INFO] Starting workers"
start-slave.sh spark://$ip_address:7077
echo "[INFO] Starting history server"
start-history-server.sh
echo "[INFO] Spark was started at $ip_address!"
