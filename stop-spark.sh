#!/bin/bash
echo "[INFO] Stopping history server"
stop-history-server.sh
echo "[INFO] Stopping workers"
stop-slave.sh
echo "[INFO] Stopping master"
stop-master.sh
echo "[INFO] Spark was stopped."
